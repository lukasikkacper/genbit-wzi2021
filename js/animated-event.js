const animateCSS = (element, animation, prefix = 'animate__') =>

    new Promise((resolve, reject) => {
        const animationName = `${prefix}${animation}`;
        const node = $(element);


        node.addClass(`${prefix}animated`);
        node.addClass(animationName);

        function handleAnimationEnd(event) {
            event.stopPropagation();

            node.removeClass(`${prefix}animated`);
            node.removeClass(animationName);

            resolve();
        }

        node.on('animationend', { once: true }, handleAnimationEnd);
    });
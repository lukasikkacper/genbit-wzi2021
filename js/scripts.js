(function($) {
    "use strict";

    var toggled = false;
    var oldToggled = false;

    $(window).bind('scroll');

    AOS.init();


    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (
            location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ?
                target :
                $("[name=" + this.hash.slice(1) + "]");
        }
    });


    $(".js-scroll-trigger").click(function() {
        $(".navbar-collapse").collapse("hide");
    });


    $("body").scrollspy({
        target: "#mainNav",
        offset: 74,
    });


    var navbarCollapse = function() {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
            toggled = true;
        } else {
            $("#mainNav").removeClass("navbar-shrink");
            toggled = false;
        }

        if (window.innerWidth > 991) {
            if (toggled == true && oldToggled != toggled) {
                animateCSS('.logo-uph', 'fadeOutUp').then((ev) => {
                    $('.logo-uph').addClass('d-none');
                });

                oldToggled = true;
            } else if (toggled == false && oldToggled != toggled) {
                $('.logo-uph').removeClass('d-none');
                animateCSS('.logo-uph', 'fadeInDown');

                oldToggled = false;
            }
        } else {
            $('.logo-uph').addClass('d-none');
        }
    };

    navbarCollapse();

    $(window).scroll(navbarCollapse);
})(jQuery);